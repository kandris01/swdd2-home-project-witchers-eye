﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Witcher_s_Eye
{
    class MapGenerator
    {
        City city;
        RawCityDatas rawCityDatas;
        Random RANGEN = new Random();

        string singleLineData;
        string textFileData;

        string regionName;
        string cityName;
        int position;
        int[] adjecentCities;
        string[] missionNames;
        int[] goldRewards;
        string[] monsterNames;

        List<string> usedCities = new List<string>();
        List<int> usedPositions = new List<int>();

        public City Generate(int size)
        {
            rawCityDatas = new RawCityDatas();
            adjecentCities = new int[1];
            missionNames = new string[1];
            goldRewards = new int[1];
            monsterNames = new string[1];

            GenerateRegionCityNames();
            GeneratePosition(size);

            adjecentCities[0] = 0;
            missionNames[0] = "0";
            goldRewards[0] = 0;
            monsterNames[0] = "0";

            city = new City(regionName, cityName, position, adjecentCities, missionNames, goldRewards, monsterNames);

            return city;
        }

        public void CreateTextFile(City[] cities, string path)
        {
            StreamWriter writer = new StreamWriter(path);
            for (int i = 0; i < cities.Length; i++)
            {
                writer.Write($"{cities[i].regionName};{cities[i].cityName};{cities[i].position}");
                writer.WriteLine();
            }
            writer.Close();
        }

        void GenerateRegionCityNames()
        {
            string tempRegion;
            string tempCity = "Not available city";
            int random = 0;

            random = RANGEN.Next(rawCityDatas.regions.Length);
            tempRegion = rawCityDatas.regions[random];
            switch (tempRegion)
            {
                case "Skellige":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.skelligeCities[RANGEN.Next(rawCityDatas.skelligeCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.skelligeCities[RANGEN.Next(rawCityDatas.skelligeCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                case "Novigrad":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.novigradCities[RANGEN.Next(rawCityDatas.novigradCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.novigradCities[RANGEN.Next(rawCityDatas.novigradCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                case "Royal Palace in Vizima":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.royalPalaceInVizimaCities[RANGEN.Next(rawCityDatas.royalPalaceInVizimaCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.royalPalaceInVizimaCities[RANGEN.Next(rawCityDatas.royalPalaceInVizimaCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                case "Velen":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.velenCities[RANGEN.Next(rawCityDatas.velenCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.velenCities[RANGEN.Next(rawCityDatas.velenCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                case "Kaer Morhen":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.kaerMorhenCities[RANGEN.Next(rawCityDatas.kaerMorhenCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.kaerMorhenCities[RANGEN.Next(rawCityDatas.kaerMorhenCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                case "Toussaint":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.toussiantCities[RANGEN.Next(rawCityDatas.toussiantCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.toussiantCities[RANGEN.Next(rawCityDatas.toussiantCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                case "White Orchard":
                    if (usedCities.Count > 0)
                    {
                        for (int i = 0; i < usedCities.Count; i++)
                        {
                            tempCity = rawCityDatas.whiteOrchardCities[RANGEN.Next(rawCityDatas.whiteOrchardCities.Length)];
                            if (usedCities[i] != tempCity)
                            {
                                usedCities.Add(tempCity);
                            }
                        }
                    }
                    else
                    {
                        tempCity = rawCityDatas.whiteOrchardCities[RANGEN.Next(rawCityDatas.whiteOrchardCities.Length)];
                        usedCities.Add(tempCity);
                    }
                    break;

                default:
                    tempRegion = "Not Available Region";
                    tempCity = "Not Available City";
                    break;
            }

            regionName = tempRegion;
            cityName = tempCity;
        }

        void GeneratePosition(int size)
        {
            int tempPosition = 0;
            bool searchResult = true;

            if (usedPositions.Count > 0)
            {
                while (searchResult)
                {
                    CheckPositions(ref tempPosition, ref searchResult, size);
                }

                usedPositions.Add(tempPosition);
                position = tempPosition;
            }
            else
            {
                tempPosition = RANGEN.Next(size);
                usedPositions.Add(tempPosition);
                position = tempPosition;
            }
        }

        void CheckPositions(ref int tempPosition, ref bool searchResult, int size)
        {
            tempPosition = RANGEN.Next(size+2);
            for (int i = 0; i < usedPositions.Count; i++)
            {
                if (usedPositions[i] == tempPosition)
                {
                    return;
                }
                else
                {
                    searchResult = false;
                }
            }
        }
    }
}
