﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Witcher_s_Eye
{
    interface ITextOutput
    {
        void AllDataToConsole(City c);

        void ChangeConsoleColor();
    }
}
