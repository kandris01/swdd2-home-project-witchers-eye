﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Witcher_s_Eye
{
    class City : ITextOutput
    {
        Random RANGEN = new Random();

        public string regionName;
        public string cityName;
        public int position;
        public int[] adjecentCities;
        public string[] missionNames;
        public int[] goldRewards;
        public string[] monsterNames;

        public City(string regionName, string cityName, int position, int[] adjecentCities, string[] missionNames, int[] goldRewards, string[] monsterNames)
        {
            this.regionName = regionName;
            this.cityName = cityName;
            this.position = position;
            this.adjecentCities = adjecentCities;
            this.missionNames = missionNames;
            this.goldRewards = goldRewards;
            this.monsterNames = monsterNames;
        }

        public void AllDataToConsole(City city)
        {
            System.Threading.Thread.Sleep(1000);
            
            //Get the Parameter's city's neighbours and put those into a single string, with commas
            string tempAdjecentCities = "";
            for (int i = 0; i < city.adjecentCities.Length; i++)
            {
                tempAdjecentCities += Convert.ToInt32(adjecentCities[i]);
                if (i+1 < city.adjecentCities.Length)
                {
                    tempAdjecentCities += ", ";
                }
                
            }

            //Get the Parameter's city's mission names and put those into a single string, with commas
            string tempMissionNames = "";
            for (int i = 0; i < city.missionNames.Length; i++)
            {
                tempMissionNames += missionNames[i];
                if (i + 1 < city.missionNames.Length)
                {
                    tempMissionNames += ", ";
                }

            }

            //Get the Parameter's city's gold rewards and put those into a single string, with commas
            string tempGoldRewards = "";
            for (int i = 0; i < city.goldRewards.Length; i++)
            {
                tempGoldRewards += Convert.ToInt32(goldRewards[i]);
                if (i + 1 < city.goldRewards.Length)
                {
                    tempGoldRewards += ", ";
                }

            }

            //Get the Parameter's city's monster names and put those into a single string, with commas
            string tempMonsterNames = "";
            for (int i = 0; i < city.monsterNames.Length; i++)
            {
                tempMonsterNames += monsterNames[i];
                if (i + 1 < city.monsterNames.Length)
                {
                    tempMonsterNames += ", ";
                }

            }

            //Write out every information about the parameter city
            Console.WriteLine($"Region Name: {city.regionName} \n City Name: {city.cityName} \n Location: {city.position} \n" +
                              $" Neighbour City Locations: {tempAdjecentCities} \n Missions: {tempMissionNames}, \n Rewards(in gold): {tempGoldRewards} \n Monsters: {tempMonsterNames} \n");
        }

        public void ChangeConsoleColor()
        {
            string currentColor = Console.ForegroundColor.ToString();
            //Create a random color for writing out different cities
            int ran = RANGEN.Next(16);
            Console.ForegroundColor = ConsoleColor.White;
            switch (ran)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    break;

                case 2:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;

                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    break;

                case 4:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    break;

                case 5:
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    break;

                case 6:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;

                case 7:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;

                case 8:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;

                case 9:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;

                case 10:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;

                case 11:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;

                case 12:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case 13:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;

                case 14:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;


                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }

            //Checks if the current Console color is the same as before and if it's true then it runs itself again until iit will be something else
            if (Console.ForegroundColor.ToString() == currentColor)
            {
                ChangeConsoleColor();
            }
        }
    }
}
