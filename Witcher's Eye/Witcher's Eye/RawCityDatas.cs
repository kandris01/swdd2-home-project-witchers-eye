﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Witcher_s_Eye
{
    class RawCityDatas
    {
        Random RANGEN = new Random();

        public string[] regions;

        public string[] skelligeCities;
        public string[] novigradCities;
        public string[] royalPalaceInVizimaCities;
        public string[] velenCities;
        public string[] kaerMorhenCities;
        public string[] toussiantCities;
        public string[] whiteOrchardCities;

        public int[] positions;

        public RawCityDatas()
        {
            AddRegions();
            AddCities();
            AddPositions();
        }

        void AddRegions()
        {
            regions = new string[] { "Skellige","Novigrad","Royal Palace in Vizima","Velen","Kaer Morhen", "Toussaint","White Orchard"};
        }

        void AddCities()
        {
            skelligeCities = new string[] {"Ard Skellige", "An Skellige","Faroe","Hindarsfjall", "Nava Isle", "Spikeroog" ,"Undvik", "Snidhall Isle", "Tengelstrand Isle"}; //Ready
            novigradCities = new string[] { "Grassy Knoll" ,"Oxenfurt" ,"Gustfields" ,"Novigrad City" }; //Ready
            royalPalaceInVizimaCities = new string[] { "Royal Palace" }; //Ready
            velenCities = new string[] { "Grayrocks", "Crow's Perch", "Spitfire Bluff", "Mudplough", "Bald Mountain", "Crookback Bog", "The Mire", "The Descent" }; //Ready
            kaerMorhenCities = new string[] { "Kaer Morhen" }; //Ready
            toussiantCities = new string[] { "Beauclair", "Caroberta Woods", "The Champs-Désolés", "Dun Tynne", "Gorgon Foothills", "Sansretour Marsh", "Sansretour Valley", "Vedette Valley" }; //Ready
            whiteOrchardCities = new string[] { "White Orchard" }; //Ready
        }

        void AddPositions()
        {
            int everyCity = skelligeCities.Length + novigradCities.Length;
            positions = new int[everyCity];
            int tempRandom = RANGEN.Next(positions.Length);

            for (int i = 0; i < positions.Length; i++)
            {
                for (int j = 0; j < positions.Length; j++)
                {
                    if (positions[j] != tempRandom)
                    {
                        positions[i] = tempRandom;
                    }
                }
            }
        }
    }
}
