﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Witcher_s_Eye
{
    class Input
    {
        City[] city;
        string[] rawData;
        string[] data;

        public City[] GetInput(string inputName)
        {
            city = new City[File.ReadAllLines(inputName).Length];
            rawData = new string[File.ReadAllLines(inputName).Length];
            int n = 0;
            foreach (var item in File.ReadAllLines(inputName))
            {
                data = item.Split(';');

                int[] tempAdjecentCities = new int[data[3].Split(',').Length];
                for (int i = 0; i < data[3].Split(',').Length; i++)
                {
                    tempAdjecentCities[i] = Convert.ToInt32(data[3].Split(',')[i]);
                }

                string[] tempMissionNames = new string[data[4].Split(',').Length];
                for (int i = 0; i < data[4].Split(',').Length; i++)
                {
                    tempMissionNames[i] = data[4].Split(',')[i];
                }

                int[] tempGoldRewards = new int[data[5].Split(',').Length];
                for (int i = 0; i < data[5].Split(',').Length; i++)
                {
                    tempGoldRewards[i] = Convert.ToInt32(data[5].Split(',')[i]);
                }

                string[] tempMonsterNames = new string[data[6].Split(',').Length];
                for (int i = 0; i < data[6].Split(',').Length; i++)
                {
                    tempMonsterNames[i] = data[6].Split(',')[i];
                }

                city[n] = new City(data[0], data[1], Convert.ToInt32(data[2]), tempAdjecentCities, tempMissionNames, tempGoldRewards, tempMonsterNames);
                n++;
            }

            return city;
        }
    }
}
