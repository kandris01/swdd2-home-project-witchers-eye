﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Witcher_s_Eye
{
    class Program
    {
        static void Main(string[] args)
        {
            City[] importCity = new City[0];
            int generateSize = 10; //Console.Readline();

            MapGenerator mapGenerator = new MapGenerator();
            City[] generatedCities = new City[generateSize];


            ////Generated cities
            //for (int i = 0; i < generatedCities.Length; i++)
            //{
            //    generatedCities[i] = mapGenerator.Generate(generateSize);
            //    generatedCities[i].ChangeConsoleColor();
            //    generatedCities[i].AllDataToConsole(generatedCities[i]);
            //}

            //for (int i = 0; i < inputCities.Length; i++)
            //{
            //    mapGenerator.CreateTextFile(generatedCities, "OutputTest01.txt");
            //}

            int menu = 0;
            do
            {
                switch (menu)
                {
                    

                    case 0:
                        WriteMenuConsole(ref menu);
                        break;

                    case 1:
                        MapGeneratorConsole(ref menu);
                        break;

                    case 2:
                        ImportCity(ref menu, ref importCity);
                        break;

                    case 3:
                        InformationOfCityToConsole(ref menu, importCity, generatedCities);
                        break;

                    case 13:
                        Console.WriteLine("Stay safe Geralt!");
                        Thread.Sleep(1200);
                        return;

                    default:
                        break;
                }
            } while (true);

        }

        static void WriteMenuConsole(ref int menu)
        {
            Console.Clear();
            Console.WriteLine("|1. Map Generator|\n|2. Import City|\n|3. Informations about cities|\n|4. Show city connections|\n|5. Start the adventure|\n|13. See you soon...|");
            string tempMenu = Console.ReadLine();
            if (tempMenu == null || tempMenu == "")
            {
                return;
            }
            else
            {
                menu = Convert.ToInt32(tempMenu);
            }
        }

        static void MapGeneratorConsole(ref int menu)
        {
            Console.Clear();
            Console.WriteLine("|MAP GENERATOR|");
        }

        static void ImportCity(ref int menu, ref City[] importCity)
        {
            Console.Clear();
            Console.WriteLine("|IMPORT CITY|");
            Console.WriteLine("Please give the name of the file you would like to import:");
            string inputFile = Console.ReadLine();
            bool falseImport = false;

            try
            {
                importCity = new City[File.ReadAllLines(inputFile).Length];
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Do not worry Geralt, it happens to all of us.");
                Console.WriteLine("Do you want to try with another input? (yes/no)");
                string exceptionAnswer = Console.ReadLine();
                switch (exceptionAnswer)
                {
                    case "yes":
                        inputFile = Console.ReadLine();
                        try
                        {
                            importCity = new City[File.ReadAllLines(inputFile).Length];
                        }
                        catch (Exception ee)
                        {
                            Console.WriteLine(ee.Message);
                            Console.WriteLine("Alright it's still okay just give me another file name maybe that will work or leave and try to generate a city.");
                            Console.WriteLine("Do you want to try again? (yes/no)");
                            exceptionAnswer = Console.ReadLine();
                            if (exceptionAnswer == "yes")
                            {
                                goto case "yes";
                            }
                            else
                            {
                                WriteMenuConsole(ref menu);
                            }
                        }
                        break;

                    case "no":
                        Console.WriteLine("Are you sure?");
                        Console.WriteLine("You can try once again if you want to.");
                        Console.WriteLine("Do you want to try it again? (alright/so long)");
                        exceptionAnswer = Console.ReadLine();
                        if (exceptionAnswer == "alright")
                        {
                            goto case "yes";
                        }
                        else
                        {
                            WriteMenuConsole(ref menu);
                        }
                        break;

                    default:
                        break;
                }
            }
            
            Input input = new Input();
            importCity = input.GetInput(inputFile);

            if (falseImport)
            {
                Console.WriteLine("The imported file was not in a correct form or was not available!");
            }
            else
            {
                Console.WriteLine("Successful import!");
                Console.WriteLine("Would you like to see your city? (yes/no)");
                string answer = Console.ReadLine();
                if (answer.ToLower().Trim() == "yes")
                {
                    CityDataToConsole(importCity);
                    Console.ReadLine();
                    WriteMenuConsole(ref menu);
                }
                else
                {
                    WriteMenuConsole(ref menu);
                }
            }
            
        }

        static void CityDataToConsole(City[] city)
        {
            for (int i = 0; i < city.Length; i++)
            {
                city[i].ChangeConsoleColor();
                city[i].AllDataToConsole(city[i]);
            }
        }

        static void InformationOfCityToConsole(ref int menu, City[] importCity, City[] generateCity)
        {
            Console.Clear();
            Console.WriteLine("|INFORMATIONS ABOUT CITIES|");
            Console.WriteLine("Please be sure to have an imported or generated city because if you try it with an empty city it won't show any useful informations.");
            Console.WriteLine("/1. Neighbour finder/\n/2. Mission finder/\n/5. Leave");
            int inforamtionsMenu = 0;
            inforamtionsMenu = Convert.ToInt32(Console.ReadLine());

            switch (inforamtionsMenu)
            {
                case 1:
                    NeighbourFinder(ref menu, importCity, generateCity);
                    break;

                case 2:
                    MissionFinder(ref menu, importCity, generateCity);
                    break;

                case 5:
                    WriteMenuConsole(ref menu);
                    return;

                default:
                    break;
            }

        }

        static void NeighbourFinder(ref int menu, City[] importedCity, City[] generatedCity)
        {
            Console.Clear();
            Console.WriteLine("|INFORMATIONS ABOUT CITIES|");
            Console.WriteLine("Neigbour finder.");
            Console.WriteLine("So do you want to use the imported or the generated city? (imported/generated)");
            Informations info = new Informations();
            string answer = Console.ReadLine();
            Console.WriteLine("Now give the position of the examined city.");
            int positionAnswer = Convert.ToInt32(Console.ReadLine());

            if (answer.ToLower().Trim() == "imported")
            {
                info.GiveAdjacentCities(importedCity, positionAnswer);
                Console.ReadLine();
                InformationOfCityToConsole(ref menu, importedCity, generatedCity);
            }
            else if (answer.ToLower().Trim() == "generated")
            {
                info.GiveAdjacentCities(generatedCity, positionAnswer);
                Console.ReadLine();
                InformationOfCityToConsole(ref menu, importedCity, generatedCity);
            }
        }

        static void MissionFinder(ref int menu, City[] importedCity, City[] generatedCity)
        {
            Console.Clear();
            Console.WriteLine("|INFORMATIONS ABOUT CITIES|");
            Console.WriteLine("Mission finder.");
            Console.WriteLine("Now you need to choose if you would like to use the imported or the generated city? (imported/generated)");
            string answer = Console.ReadLine();
            Console.WriteLine("Now the city's location you want to list the missions from.");
            int positionAnswer = Convert.ToInt32(Console.ReadLine());
            Informations info = new Informations();

            if (answer.ToLower().Trim() == "imported")
            {
                info.MissionFinder(importedCity, positionAnswer);
                Console.ReadLine();
                InformationOfCityToConsole(ref menu, importedCity, generatedCity);
            }
            else if (answer.ToLower().Trim() == "generated")
            {
                info.MissionFinder(generatedCity, positionAnswer);
                Console.ReadLine();
                InformationOfCityToConsole(ref menu, importedCity, generatedCity);
            }
        }
    }
}
