﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Witcher_s_Eye
{
    class Informations
    {

        public void GiveAdjacentCities(City[] city, int location)
        {
            List<City> neighbours = new List<City>();
            int[] neighboursIndex = new int[0];

            for (int i = 0; i < city.Length; i++)
            {
                if (location == city[i].position)
                {
                    neighboursIndex = new int[city[i].adjecentCities.Length];
                    for (int j = 0; j < city[i].adjecentCities.Length; j++)
                    {
                        neighboursIndex = city[i].adjecentCities;
                    }
                }
            }

            Console.WriteLine("The neighbours of the requested city are:");
            for (int i = 0; i < neighboursIndex.Length; i++)
            {
                for (int j = 0; j < city.Length; j++)
                {
                    if (city[j].position == neighboursIndex[i])
                    {
                        Console.WriteLine(city[j].cityName);
                    }
                }
            }
        }

        public void MissionFinder(City[] city, int location)
        {
            City currentCity = new City(null,null,0,null,null,null,null);
            for (int i = 0; i < city.Length; i++)
            {
                if (city[i].position == location)
                {
                    currentCity = new City(city[i].regionName, city[i].cityName, city[i].position, city[i].adjecentCities, city[i].missionNames, city[i].goldRewards, city[i].monsterNames);
                }
            }

            for (int i = 0; i < currentCity.missionNames.Length; i++)
            {
                Console.WriteLine(currentCity.missionNames[i]+" - "+currentCity.goldRewards[i]+" gold");
            }
        }

        public void ShowCityNetwork()
        {

        }
    }
}
